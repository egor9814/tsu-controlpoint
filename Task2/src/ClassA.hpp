//
// Created by egor9814 on 02 Nov 2020.
//

#ifndef CONTROLPOINT_CLASSA_HPP
#define CONTROLPOINT_CLASSA_HPP

class ClassB;
class ClassA
{
private:
	bool m_update;
	ClassB* m_pB[4]{nullptr}; //массив указателей на связанные объекты
public:
	ClassA();
	~ClassA();
	//вернуть количество связей
	int getMultiplicity() const;
	//вернуть указатели на связанные объекты
	const ClassB** getB() const;
	bool hasB() const; // проверить наличие связей
	bool hasB(const ClassB&) const; //проверить связь
	void addB(ClassB&);//установить связь
	void removeB(ClassB&);//разорвать связь
	void removeB(); //разорвать все связи
};

#endif //CONTROLPOINT_CLASSA_HPP
