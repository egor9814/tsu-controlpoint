//
// Created by egor9814 on 02 Nov 2020.
//

#ifndef CONTROLPOINT_CLASSB_HPP
#define CONTROLPOINT_CLASSB_HPP

class ClassA;
class ClassB {
	ClassA* m_pA; //указатель на связанный объект
	bool m_update; //признак обновления связи
public:
	ClassB();
	~ClassB(); //вернуть указатель на связанный объект
	const ClassA* getA() const;
	bool hasA() const;//проверить наличие связи
	void addA(ClassA&);//установить связь
	void removeA(); //разрушить связь
};

#endif //CONTROLPOINT_CLASSB_HPP
