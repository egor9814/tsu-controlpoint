//
// Created by egor9814 on 02 Nov 2020.
//

#include "ClassA.hpp"
#include "ClassB.hpp"

ClassA::ClassA() : m_update(false)
{
	// цикл заменен на иницализатор
}
ClassA :: ~ClassA()
{
	for (auto &it : m_pB)
		it = nullptr;
}
//вернуть количество связей
int ClassA:: getMultiplicity() const
{
	int n(0);
	while (n < 4 && m_pB[n])
		++n;
	return n;
}
//вернуть указатели на связанные объекты
const ClassB** ClassA:: getB() const
{
	return const_cast <const ClassB**> (m_pB);
}
//проверить наличие связей
bool ClassA:: hasB() const
{
	return getMultiplicity() > 0;
}
//проверить связь с объектом
bool ClassA:: hasB(const ClassB& r) const
{
	int j(0);
	while (j < 4 && m_pB[j])
	{
		if (m_pB[j] == &r)
			return true;
		j++;
	}
	return false;
}
//установить связь с объектом
void ClassA::addB(ClassB& r)
{
	if (hasB(r))
		return;
	if (m_update)
		return;
	//должно быть место для добавления
	if (getMultiplicity() >= 4) return;
	//r должен быть свободен
	if (r.hasA())
		return;
	m_update = true;
	//запрос r об обновлении связи
	r.addA(*this);
	//обновление своих связей
	int n = getMultiplicity();
	m_pB[n] = &r;
	m_update = false;
}
//разорвать связь с объектом
// была беда с названием: идентификатор метода содержала non-ASCII символы
void ClassA ::removeB(ClassB& r)
{
	if (m_update) return;
	if (!hasB(r))
		return;
	m_update = true;
	//запрос r о разрыве связи
	r.removeA();
	//разрыв своей связи
	int n = getMultiplicity();
	int j;
	for (j = 0; j < n && m_pB[j] != &r; j++);
	n--;
	for (int i(j); i < n; i++)
		m_pB[i] = m_pB[i+1];
	m_pB[n] = nullptr;
	m_update = false;
}
//разорвать все связи
void ClassA:: removeB()
{
	if (m_update) return;
	m_update = true;
	for (int j(0); j < getMultiplicity(); j++)
		m_pB[j]->removeA();
	for (auto &it : m_pB)
		it = nullptr;
	m_update = false;
}
