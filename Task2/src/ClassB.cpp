//
// Created by egor9814 on 02 Nov 2020.
//

#include "ClassB.hpp"
#include "ClassA.hpp"

ClassB :: ClassB(): m_update (false), m_pA(nullptr) {}
ClassB :: ~ClassB()
{
	m_pA = nullptr;
}
//вернуть указатель на связанный объект
const ClassA* ClassB :: getA() const
{
	return m_pA;
}
//проверить наличие связи
bool ClassB :: hasA() const
{
	return m_pA != nullptr;
}
//установить связь
void ClassB :: addA (ClassA& r)
{
	if (m_update) return;
	if (m_pA == &r) return;
	//объект r должен быть свободен от этого объекта
	if (r.hasB(*this)) return;
	//разрушение своей связи
	if (hasA()) removeA();
	//модификация связи
	m_update = true;
	r.addB(*this);
	m_pA = &r;
	m_update = false;
}
//разрушить связь
void ClassB :: removeA()
{
	if (!hasA()) return;
	if (m_update) return;
	m_update = true;
	//разрушение своей связи
	// разрывая связь с m_pA, не думаю что нужно разорвать ВСЕ связи, поэтому разорвем только одну текущую
	m_pA->removeB(*this);
	m_pA = nullptr;
	m_update = false;
}
