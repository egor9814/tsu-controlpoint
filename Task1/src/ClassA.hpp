//
// Created by egor9814 on 02 Nov 2020.
//

#ifndef CONTROLPOINT_CLASSA_HPP
#define CONTROLPOINT_CLASSA_HPP

class ClassB;

class ClassA
{
	int m_n; //фактическое число частей
	ClassB* m_pB[100]{nullptr}; //массив указателей на части
public:
	ClassA();
	~ClassA();
	const int &getN () const; //вернуть число частей
	const ClassB** getB (int&) const; //вернуть части
	bool add (const int &); //добавить часть
	bool del (const int &); //удалить часть
	int find (const int &) const; //вернуть индекс части
	int getX(const int &) const;//вернуть значение объекта
};

#endif //CONTROLPOINT_CLASSA_HPP
