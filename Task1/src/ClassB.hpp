//
// Created by egor9814 on 02 Nov 2020.
//

#ifndef CONTROLPOINT_CLASSB_HPP
#define CONTROLPOINT_CLASSB_HPP

class ClassB
{
	int m_x; //член-данное части

public:
	ClassB();
	~ClassB();
	//методы установки и получения значения m_х
	void setX(const int &);
	const int &getX() const;
	bool verify (const int &) const; //проверить объект
	ClassB& operator = (const ClassB&); //перегрузка оператора =
	//перегрузка операторов == и !=
	bool operator == (const ClassB&) const;
	bool operator != (const ClassB&) const;
};

#endif //CONTROLPOINT_CLASSB_HPP
