//
// Created by egor9814 on 02 Nov 2020.
//

#include <stdexcept>
#include "ClassA.hpp"
#include "ClassB.hpp"

ClassA::ClassA() : m_n(0) {}
ClassA :: ~ClassA()
{
	for (int j(0); j < m_n; j++)
		delete m_pB[j];
}
//вернуть количество частей
const int &ClassA :: getN() const {return m_n;}
//вернуть части const
const ClassB** ClassA :: getB (int& n) const
{
	n = m_n;
	return const_cast <const ClassB**> (m_pB);
}
//добавить часть
//метод получает все необходимые параметры для создания части
bool ClassA :: add (const int &x)
{
	// место закончилось, значит мы не можем добавить
	if (m_n >= 100)
		return false;
	if (find(x) >= 0) return false;
	m_pB[m_n] = new ClassB;
	m_pB[m_n]->setX(x);
	m_n++; return true;
}
//удалить часть
bool ClassA :: del(const int &key)
{
	//поиск индекса части
	int JDel = find (key);
	//часть не найдена
	if (JDel < 0)
		return false;
	//разрушение части
	delete m_pB[JDel];
	//сжатие массива
	m_n--; // (чуточку оптимизации)
	while (JDel < m_n)
	{
		m_pB[JDel] = m_pB[JDel+1];
		JDel++;
	}
	return true;
}
//вернуть индекс части
int ClassA :: find (const int &key) const
{
	for (int j(0); j < m_n; j++)
		if (m_pB[j]->verify(key))
			return j;
	return -1;
}
//вернуть значение объекта с индексом
// вызывается метод класса частей m_pB[j]->getX();
int ClassA::getX(const int &j) const
{
	if (0 <= j && j < m_n)
		return m_pB[j]->getX();
	throw std::runtime_error("index out of bounds");
}
