//
// Created by egor9814 on 02 Nov 2020.
//

/*
 * 1. Познакомьтесь с предложенной реализацией классов и
 *    продемонстрируйте работу предложенной реализации классов на некотором примере.
 * 2. Определите тип отношения между классами, ответ обоснуйте.
 * */

#include <cstdlib>
#include <iostream>

#include "ClassA.hpp"
#include "ClassB.hpp"

int main() {
	/* Отношение между классами: Композиция с ограниченной кратностью(100)
	 * (изображено в Task1.png), т.к. класс ClassA содержит массив указателей
	 * на класс ClassB размером 100, а класс ClassB не содержит ни одного поля,
	 * ссылающегося каким-либо образом на класс ClassA. */

	/* Из реализации метода ClassA::add(int), понятно что добавляются
	 * только части с уникальным значением ClassB::m_x, соответственно
	 * очевидным вариантом использования класса ClassA - множествово
	 * уникальных целых чисел (как std::set<int>, но подругому) */
	/* Также сохраняется порядок добавления элементов, к примеру
	 * добавив 1, 4, 2, 1, 2, 1; массив будет содержать части: 1, 4, 2 */

	/* Самый простой пример: сгенерировать N случайных уникальных чисел, и вывести их,
	 * где 10 <= N <= 100 */
	ClassA instance;

	srand(static_cast<unsigned int>(time(nullptr)));
	for (auto n = instance.getN(); n < 10; n = instance.getN()) {
		auto count = 100 - instance.getN();
		for (int i = 0; i < count; i++) {
			// генерируем псевдослучайные числа в диапазоне [0;19], соответсвенно
			// ункикальных будет всего 20 штук
			instance.add(rand() % 20);
		}
	}

	auto n = instance.getN();
	for (int i = 0; i < n; i++) {
		std::cout << instance.getX(i) << "; ";
	}
	std::cout << std::endl;

	/* А теперь удалим четные числа (стоимость операции в худшем случае O(n*n*(n-1)),
	 * n-1 - кол-во присвоений при сдвиге, n - ClassA::find внутри ClassA::del,
	 * и n - проверок на четность) */
	n = instance.getN();
	for (int i = 0; i < n; i++) {
		auto x = instance.getX(i);
		if (x % 2 == 0) {
			instance.del(x);
			n--; // потому что кол-во частей уменьшится
			i--; // потому что перед декрементом i-ая часть - следующая
		}
	}

	n = instance.getN();
	for (int i = 0; i < n; i++) {
		std::cout << instance.getX(i) << "; ";
	}
	std::cout << std::endl;

	// И проверим на выход за пределы массива ClassA::m_pB
	try {
		instance.getX(1000);
		std::cout << "fail-1!" << std::endl;
	} catch (const std::exception &) {
		std::cout << "success-1!" << std::endl;
	}
	try {
		instance.getX(-1);
		std::cout << "fail-2!" << std::endl;
	} catch (const std::exception &) {
		std::cout << "success-2!" << std::endl;
	}

	return 0;
}
